'''
Template Component main class.

'''

import csv
import json
import logging
import os
import sys

from kbc.env_handler import KBCEnvHandler
from kbc.result import KBCTableDef, KBCResult

from adform.api_service import AdformClient

# configuration variables
KEY_API_TOKEN = '#api_secret'
KEY_API_CLIENT_ID = 'api_client_id'

KEY_LINE_ITEMS = 'line_items'
KEY_LISTS = 'lists'

KEY_FILE_NAME = "file_name"
KEY_METHOD = "method"
KEY_TYPE = "type"

# #### Keep for debug
KEY_DEBUG = 'debug'
MANDATORY_PARS = [KEY_API_TOKEN, KEY_API_CLIENT_ID]
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.0.1'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS, )
        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True

        log_level = logging.DEBUG if debug else logging.INFO
        # setup GELF if available
        if os.getenv('KBC_LOGGER_ADDR', None):
            self.set_gelf_logger(log_level)
        else:
            self.set_default_logger(log_level)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config()
            if self.cfg_params.get(KEY_LINE_ITEMS):
                for m in self.cfg_params[KEY_LINE_ITEMS]:
                    self.validate_parameters(m, [KEY_FILE_NAME, KEY_METHOD], KEY_LINE_ITEMS)
            if self.cfg_params.get(KEY_LISTS):
                for m in self.cfg_params[KEY_LISTS]:
                    self.validate_parameters(m, [KEY_FILE_NAME, KEY_TYPE], KEY_LISTS)
        except ValueError as e:
            logging.exception(e)
            exit(1)

        # intialize instance parameteres
        try:
            if self.cfg_params[KEY_API_TOKEN]:
                # legacy client credential flow support
                self.client = AdformClient('')
                self.client.login_using_client_credentials(self.cfg_params[KEY_API_CLIENT_ID],
                                                           self.cfg_params[KEY_API_TOKEN],
                                                           scope='https://api.adform.com/scope/buyer.rtb.lineitem')
            else:
                # oauth
                auth = json.loads(self.get_authorization()['#data'])
                self.client = AdformClient(auth.get('access_token'))
        except Exception as ex:
            raise RuntimeError(f'Login failed, please check your credentials! {str(ex)}') from ex

    def run(self):
        '''
        Main execution code
        '''
        params = self.cfg_params  # noqa

        line_items = params.get(KEY_LINE_ITEMS, [])
        lists = params.get(KEY_LISTS, [])
        in_tables = self.get_input_tables_definitions()
        self._validate_inputs(lists, line_items, [t.file_name for t in in_tables])

        list_log = os.path.join(self.tables_out_path, 'list_log.csv')
        with open(list_log, mode='w+', encoding='utf-8', newline='') as log_file:
            writer = csv.DictWriter(log_file, ['result_id', 'list_type', 'data'])
            writer.writeheader()
            for lst in lists:
                self.write_lists(lst, writer)

        for lit in line_items:
            self.write_line_item(lit)

        logging.info('Write finished successfully!')

    def store_results(self, report_result, incremental=True, report_name='result_data', pkey=[]):

        file_name = report_name + '.csv'
        res_file_path = os.path.join(self.tables_out_path, file_name)
        if os.path.exists(res_file_path):
            mode = 'a'
        else:
            mode = 'w+'
        columns = report_result['reportData']['columnHeaders']
        with open(res_file_path, mode, newline='', encoding='utf-8') as out:
            writer = csv.writer(out)
            # write header
            if mode == 'w+':
                writer.writerow(columns)
            # write data
            writer.writerows(report_result['reportData']['rows'])

        table_def = KBCTableDef(name=file_name, columns=[], pk=pkey, destination='')
        result = KBCResult(file_name, res_file_path, table_def)
        self.create_manifests([result], incremental=incremental)

    def _validate_inputs(self, lists, line_items, in_tables):
        err = []
        for ls in lists:
            f_name = ls[KEY_FILE_NAME]
            if f_name not in in_tables:
                err.append(f"List input table {f_name} is missing!")
        for li in line_items:
            f_name = li[KEY_FILE_NAME]
            if f_name not in in_tables:
                err.append(f"Line Item input table {f_name} is missing!")
        if err:
            raise ValueError(f"Some input tables are missing, check the config: {err}")

        return err

    def write_lists(self, lst, writer):
        with open(self.get_input_table_by_name(lst[KEY_FILE_NAME]).full_path, mode='rt', encoding='utf-8') as in_file:
            reader = csv.DictReader(in_file)

            if {'payload'} != set(reader.fieldnames):
                raise ValueError(f"Line Item input table header {reader.fieldnames} is not equal to ['payload']")
            for row in reader:
                res = self.client.create_list(lst[KEY_TYPE], row['payload'])
                writer.writerow({"result_id": res["id"], "list_type": lst[KEY_TYPE], "data": row['payload']})

        return

    def write_line_item(self, line_item):
        with open(self.get_input_table_by_name(line_item[KEY_FILE_NAME]).full_path, mode='rt',
                  encoding='utf-8') as in_file:
            reader = csv.DictReader(in_file)

            if {'id', 'payload'} != set(reader.fieldnames):
                raise ValueError(f"Line Item input table header {reader.fieldnames} is not equal to ['id', 'payload']")

            for row in reader:
                if line_item[KEY_METHOD] == 'PUT':
                    self.client.update_line_item(row['id'], row['payload'])
                elif line_item[KEY_METHOD] == 'POST':
                    self.client.create_line_item(row['payload'])
        return


"""
    Main entrypoint
"""

if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug = sys.argv[1]
    else:
        debug = False
    try:
        comp = Component(debug)
        comp.run()
    except Exception as e:
        logging.exception(e)
        exit(1)
