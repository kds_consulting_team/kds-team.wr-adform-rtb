## Authorization

**API client secret**, **API client ID** - Adform client credentials for registered app with `Credentials flow enabled`
 and enabled for the `https://api.adform.com/v1/help/buyer/rtb/lineitems` scope. Contact the Adform support to obtain the credentials.

## Line Items

Provide input table on the input mapping and state the file name. The table must have two columns:
- `id` - line_item id
- `payload` - actual payload of the http request as defined [here](https://api.adform.com/v1/help/buyer/rtb/lineitems#!/LineItems/LineItems_Post)

## Lists

### Domain list

Provide input table with single column `payload` containing the json payload:

```json
{
  "domains": [
    "string"
  ]
}
``` 

### Cookie list

Provide input table with single column `payload` containing a CSV formatted list (including new lines):

```csv
9054558048553722051;
9054558048553722052;1.5
9054558048553722053;
```


## Output

`list_log` table containing IDs and content of created list objects.

 
| result_id  | list_type  | data  |
|---|---|---|
| resulting list ID to be linked with line item   | list type (cookie/domain)  | data - content sent to that list  |
