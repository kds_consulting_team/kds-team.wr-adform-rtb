`# Adform Reports extractor

Write line items and domain/cookie lists to Adform. Simple writer that expects that the actual api payload is provided.


**Table of contents:**  
  
[TOC]

# Configuration

## Authorization

- **API client secret**, **API client ID** - Adform client credentials for registered app with `Credentials flow` enabled.

## Line Items

Provide input table on the input mapping and state the file name. The table must have two columns:
- `id` - line_item id
- `payload` - actual payload of the http request as defined [here](https://api.adform.com/v1/help/buyer/rtb/lineitems#!/LineItems/LineItems_Post)

Example of line_item payload:

```json
{
  "id": 1,
  "name": "Test line item",
  "type": "Display",
  "placementId": 1,
  "orderId": 1,
  "campaignId": 2,
  "agencyId": 11,
  "rtbMediaId": 12,
  "paused": false,
  "deleted": false,
  "crossDeviceEnabled": false,
  "showSingleImpression": false,
  "bidOnMatchedCookies": false,
  "brandSafetyEnabled": false,
  "periods": [
    {
      "start": "2017-01-01T12:00:00+00:00",
      "end": "2017-01-11T12:00:00+00:00"
    }
  ],
  "budget": {
    "useBudgetFrom": "LineItem",
    "money": {
      "amount": 100.5,
      "pacingType": "Evenly",
      "periodType": "Total",
      "locked": false
    },
    "useParentBudget": false
  },
  "pricing": {
    "cpm": {
      "price": 5
    }
  },
  "impressionCapping": [
    {
      "id": 1,
      "usePeriodFrom": "Campaign",
      "frequency": {
        "impressions": 1000,
        "viewability": {
          "type": "BrandExposure",
          "duration": 600
        }
      }
    },
    {
      "id": 1,
      "usePeriodFrom": "LineItem",
      "period": {
        "type": "Days",
        "duration": 2
      },
      "accumulated": {
        "viewability": {
          "type": "Time",
          "duration": 600
        }
      }
    }
  ],
  "inventories": [
    {
      "id": 1,
      "buyingType": "OpenExchange",
      "bidMultiplier": 1.5,
      "deals": [
        {
          "id": "DealId",
          "bidPrice": 3.5
        }
      ],
      "packages": [
        {
          "id": 1,
          "bidMultiplier": 1.5
        }
      ]
    }
  ],
  "categories": {
    "iab": {
      "targetingMode": "Include",
      "ids": [
        1,
        2,
        3
      ]
    },
    "contextual": {
      "providerId": 1,
      "ids": [
        1,
        2,
        3
      ]
    },
    "mobile": {
      "targetingMode": "Include",
      "ids": [
        1,
        2,
        3
      ]
    }
  },
  "domains": {
    "include": {
      "source": "Template",
      "id": "33421"
    },
    "excludeUnknown": true,
    "includeRule": {
      "customListId": "",
      "templateIds": [
        33421
      ]
    }
  },
  "placement": {
    "targetAboveTheFold": true,
    "videoSettings": {
      "instream": {
        "adPositions": [
          "MidRoll"
        ]
      },
      "videoTypes": [
        "Instream",
        "Outstream"
      ],
      "letterBoxing": [
        "Allowed"
      ],
      "playback": [
        "AutoPlaySoundOff"
      ],
      "playerSizes": [
        "Hd"
      ]
    }
  },
  "targeting": [
    {
      "name": "Audience 1",
      "bidMultiplier": 1.5,
      "rules": {
        "locations": [
          {
            "targetingMode": "Include",
            "locations": [
              1,
              2,
              3
            ],
            "zipCodes": {
              "targetingMode": "Exclude",
              "codes": [
                "WX01",
                "GB02"
              ]
            }
          }
        ],
        "languages": [
          {
            "targetingMode": "Include",
            "languages": [
              "value 1",
              "value 2",
              "value 3"
            ]
          }
        ],
        "campaigns": [
          {
            "targetingMode": "Include",
            "type": "Clicked",
            "campaigns": [
              1,
              2,
              3
            ],
            "match": "All",
            "engagementTypes": [],
            "retargetingSettings": {
              "intervalType": "AtAnyTime",
              "timeUnit": "Days",
              "amount": 1
            }
          }
        ],
        "audienceTrackingPoints": [
          {
            "targetingMode": "Include",
            "trackingPoints": [
              1,
              2,
              3
            ]
          }
        ],
        "trackingPoints": [
          {
            "targetingMode": "Include",
            "trackingPoints": [
              1,
              2,
              3
            ],
            "trackingPointFilters": [
              1,
              2,
              3
            ],
            "match": "All",
            "retargetingSettings": {
              "intervalType": "AtAnyTime",
              "timeUnit": "Days",
              "amount": 1
            }
          }
        ],
        "dmpAudiences": [
          {
            "targetingMode": "Include",
            "providerId": 1,
            "categoryId": 1,
            "segments": [
              {
                "id": 1,
                "fee": 0,
                "lookalikeFee": 0,
                "crossDeviceFee": 0,
                "sources": [
                  "Original"
                ]
              },
              {
                "id": 2,
                "fee": 0,
                "lookalikeFee": 0,
                "crossDeviceFee": 0,
                "sources": [
                  "CrossDevice"
                ]
              }
            ]
          }
        ],
        "isps": [
          {
            "targetingMode": "Include",
            "isps": [
              1,
              2,
              3
            ]
          }
        ],
        "ipRanges": [
          {
            "targetingMode": "Include",
            "id": "753ae22cf806cc5a56794c9a9185f40dcc78e2aa_3"
          }
        ],
        "cookies": {
          "id": "753ae22cf806cc5a56794c9a9185f40dcc78e2aa_3"
        },
        "deviceIdentifiers": {
          "targetingMode": "Include",
          "id": "753ae22cf806cc5a56794c9a9185f40dcc78e2aa_3"
        },
        "hyperlocals": {
          "locations": [
            {
              "address": "Example address",
              "latitude": 100.1,
              "longitude": 100.2,
              "radius": 1,
              "notes": "Example notes"
            }
          ]
        },
        "connectionTypes": [
          {
            "targetingMode": "Include",
            "types": [
              "Mobile",
              "Wifi",
              "Wired"
            ],
            "subTypes": [
              "Mobile2G",
              "Mobile3G",
              "Mobile4G"
            ]
          }
        ],
        "devices": [
          {
            "targetingMode": "Include",
            "devices": [
              1,
              2,
              3
            ],
            "oses": [
              1,
              2,
              3
            ]
          }
        ],
        "devicesV2": [
          {
            "targetingMode": "Include",
            "browsers": [
              {
                "minVersion": "2.0",
                "id": 1
              },
              {
                "minVersion": "2.0",
                "maxVersion": "5.0",
                "id": 2
              },
              {
                "maxVersion": "5.0",
                "id": 3
              }
            ],
            "vendors": [
              {
                "id": 1
              },
              {
                "id": 2
              },
              {
                "id": 3
              }
            ],
            "models": [
              {
                "id": 1
              },
              {
                "id": 2
              },
              {
                "id": 3
              }
            ],
            "types": [
              {
                "id": 1
              },
              {
                "id": 2
              },
              {
                "id": 3
              }
            ],
            "platforms": [
              {
                "minVersion": "2.0",
                "id": 1
              },
              {
                "minVersion": "2.0",
                "maxVersion": "5.0",
                "id": 2
              },
              {
                "maxVersion": "5.0",
                "id": 3
              }
            ]
          }
        ],
        "mobileCarriers": [
          {
            "targetingMode": "Include",
            "mobileCarriers": [
              1,
              2,
              3
            ]
          }
        ]
      }
    }
  ],
  "banners": [
    {
      "id": "56b5b2a2-5b08-4e44-8ab6-220dbfcbe0fa",
      "bidMultiplier": 1.5
    }
  ],
  "environmentSettings": {
    "environments": [
      "DesktopWeb",
      "MobileWeb"
    ],
    "formats": [
      "Display",
      "AudioAndVideo",
      "Native"
    ]
  },
  "bidOnlyOnFullConsent": true
}
```

## Lists

### Domain list

Provide input table with single column `payload` containing the json payload:

```json
{
  "domains": [
    "string"
  ]
}
``` 

### Cookie list

Provide input table with single column `payload` containing a CSV formatted list (including new lines):

```csv
9054558048553722051;
9054558048553722052;1.5
9054558048553722053;
```

# Output

`list_log` table containing IDs and content of created list objects.

 
| result_id  | list_type  | data  |
|---|---|---|
| resulting list ID to be linked with line item   | list type (cookie/domain)  | data - content sent to that list  |


# Development
 
This example contains runnable container with simple unittest. For local testing it is useful to include `data` folder in the root
and use docker-compose commands to run the container or execute tests. 

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path:
```yaml
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
```

Clone this repository, init the workspace and run the component with following command:

```
git clone https://bitbucket.org:kds_consulting_team/kbc-python-template.git my-new-component
cd my-new-component
docker-compose build
docker-compose run --rm dev
```

Run the test suite and lint check using this command:

```
docker-compose run --rm test
```

# Integration

For information about deployment and integration with KBC, please refer to the [deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/) `